#include "drivers_api.h"
#include "validity90.h"
#include <openssl/evp.h>
#include <openssl/err.h>

static void __printGBytes(GBytes *v)
{
  gsize s;
  const guint8 *data;
  data = g_bytes_get_data(v, &s);


  for(gsize i = 0; i < s; i += 16){
    for(gint j=0;j<16 && i+j < s;++j){
      g_print("0x%x\t", data[i+j]);
    }
    g_print("\n");
  }
}

#define printGBytes(v) \
  g_print("print of %s: \n", #v); \
  __printGBytes(v);

static void
reverse_mem(GByteArray *array, gsize l, gsize r)
{
  for(gint i=l, j=r-1;i<j;++i,--j){
    guint8 tmp = array->data[i];
    array->data[i] = array->data[j];
    array->data[j] = tmp;
  }
}

static GByteArray *
validity90_handle_rsp6_pubkey_packet(GBytes *bytes, GError **error)
{
  gsize len;
  const guint8 *data = g_bytes_get_data(bytes, &len);


  GByteArray *ret = g_byte_array_sized_new(0x40);
  g_byte_array_append(ret, data + 0x4c, 0x20);
  g_byte_array_append(ret, data + 0x08, 0x20);

  reverse_mem(ret, 0, 0x40);
  return ret;
}

static GByteArray *
validity90_get_system_seed(GError **error)
{
  GFile *name_file = g_file_new_for_path("/sys/class/dmi/id/product_name");
  GBytes *name = g_file_load_bytes(name_file, NULL, NULL, error);
  printGBytes(name);
  GFile *serial_file = g_file_new_for_path("/sys/class/dmi/id/product_serial");
  GBytes *serial = g_file_load_bytes(serial_file, NULL, NULL, error);
  printGBytes(serial);

  GByteArray *seed = g_byte_array_new();

  gsize len;
  g_autofree gpointer ps1 = g_bytes_unref_to_data(name, &len);
  g_byte_array_append(seed, ps1, len);
  seed->data[seed->len-1] = 0;

  g_autofree gpointer ps2 = g_bytes_unref_to_data(serial, &len);
  g_byte_array_append(seed, ps2, len);
  seed->data[seed->len-1] = 0;
 
  return seed;
}

GByteArray *
validity90_tls_prf(const guint8 *secret, const gsize secret_len, const gchar *label, const guint8 *seed, const gsize seed_len, const gsize out_len, GError **error)
{
  GByteArray *s = g_byte_array_new();
  g_byte_array_append(s, (guint8 *)label, strlen(label));
  g_byte_array_append(s, seed, seed_len);

  GHmac *ghmac = g_hmac_new(G_CHECKSUM_SHA256, secret, secret_len);
  g_hmac_update(ghmac, s->data, s->len);

  gsize buffer_len = 0x20 + s->len, it_buff_len = 0x20;
  guint8 *buffer = g_malloc(buffer_len), *it_buff = g_malloc(it_buff_len);
  g_hmac_get_digest(ghmac, buffer, &buffer_len);
  g_assert(buffer_len == 0x20);
  memmove(buffer + 0x20, s->data, s->len);
  buffer_len += s->len;

  //reset ghmac
  g_hmac_unref(ghmac);
  ghmac = g_hmac_new(G_CHECKSUM_SHA256, secret, secret_len);

  g_hmac_update(ghmac, buffer, buffer_len);
  g_hmac_get_digest(ghmac, it_buff, &it_buff_len);
  g_assert(it_buff_len == 0x20);

  GByteArray *ret = g_byte_array_new_take(g_memdup(it_buff, it_buff_len), MIN(it_buff_len, out_len));
  while(ret->len < out_len){
    g_hmac_unref(ghmac);
    ghmac = g_hmac_new(G_CHECKSUM_SHA256, secret, secret_len);

    buffer_len = 0x20;
    g_hmac_update(ghmac, buffer, buffer_len);
    g_hmac_get_digest(ghmac, buffer, &buffer_len);
    g_assert(buffer_len == 0x20);
    buffer_len += s->len;

    g_hmac_unref(ghmac);
    ghmac = g_hmac_new(G_CHECKSUM_SHA256, secret, secret_len);

    g_hmac_update(ghmac, buffer, buffer_len);
    g_hmac_get_digest(ghmac, it_buff, &it_buff_len);
    g_assert(it_buff_len == 0x20);
    g_byte_array_append(ret, it_buff, MIN(it_buff_len, out_len - ret->len));
  }
  return ret;
}

static GByteArray *
validity90_master_key(GError **error)
{
  GByteArray *seed = validity90_get_system_seed(error);
  return validity90_tls_prf(SECRET_KEY, SECRET_KEY_LEN, "GWK", seed->data, seed->len, 0x20, error);
}

static gboolean
validity90_check_aes_padding(guint8 *buffer, gint len)
{
  gint pad_size = buffer[len - 1];
  for(gint i=0;i<pad_size;++i){
    if(buffer[len - 1 - i] != pad_size){
      return FALSE;
    }
  }

  return TRUE;
}

static GByteArray *
validity90_ecdsa_key(GBytes *bytes, GByteArray *master_key_aes, GError **error)
{
  gsize len;
  const guint8 *data = g_bytes_get_data(bytes, &len);
  g_assert(len >= 0x81);
  g_assert(data[0] == 0x02);

  EVP_CIPHER_CTX *context = EVP_CIPHER_CTX_new();
  int res = EVP_DecryptInit(context, EVP_aes_256_cbc(), master_key_aes->data, data + 0x01); //TODO
  g_assert(res);

  EVP_CIPHER_CTX_set_padding(context, 0);

  gint buffer_len = 0, buffer_len2;
  guint8 *buffer = g_malloc(0x70);

  res = EVP_DecryptUpdate(context, buffer, &buffer_len, data + 0x11, 0x70);
  if(!res){
    g_print("Failed to EVP decrypt, error: %lu, %s",
        ERR_peek_last_error(), ERR_error_string(ERR_peek_last_error(), NULL));
  }
  g_assert(res);

  res = EVP_DecryptFinal(context, buffer + buffer_len, &buffer_len2);
  g_assert(res);
  buffer_len += buffer_len2;

  g_assert(validity90_check_aes_padding(buffer, buffer_len));
  g_print("len1: %d, len2: %d\n", buffer_len, buffer_len2);

  GByteArray *ret = g_byte_array_new_take(buffer + 0x40, 0x20);
  reverse_mem(ret, 0, 0x20);
  return ret;
}

static GByteArray *
validity90_handle_rsp6_ecdsa_packet(GBytes *bytes, GError **error)
{
  GByteArray *master_key_aes = validity90_master_key(error),
             *ecdsa_key = validity90_ecdsa_key(bytes, master_key_aes, error);

  /*
  GByteArray *test = validity90_tls_prf(SECRET_KEY, SECRET_KEY_LEN, "GWKAAAAAA", master_key_aes->data, master_key_aes->len, 0x100, error);
  GBytes *testBytes = g_byte_array_free_to_bytes(test);
  printGBytes(testBytes);
  */

  return ecdsa_key;
}

static gboolean
validity90_check_hash(GBytes *ghash, GBytes *gdata)
{
  gsize len, buffer_len = 0x20;
  const guint8 *data = g_bytes_get_data(gdata, &len);
  g_autofree guint8* buffer = g_malloc(buffer_len);

  g_autoptr(GChecksum) gchecksum = g_checksum_new(G_CHECKSUM_SHA256);
  g_checksum_update(gchecksum, data, len);
  g_checksum_get_digest(gchecksum, buffer, &buffer_len);
  g_assert(buffer_len == 0x20);

  g_autoptr(GBytes) gdata_hash = g_bytes_new_take(g_steal_pointer(&buffer), buffer_len);
  return g_bytes_equal(ghash, gdata_hash);
}

void
validity90_parse_rsp6(GInputStream *stream, rsp6_info *info, GError **error)
{
  guint16 type, size;
  GBytes *hash, *data;
  GByteArray *ecdsa_q = NULL, *ecdsa_d = NULL, *ecdh = NULL;

  data = g_input_stream_read_bytes(stream, 8, NULL, error);

  while(TRUE){
    g_input_stream_read(stream, &type, 2, NULL, error);
    g_input_stream_read(stream, &size, 2, NULL, error);
    hash = g_input_stream_read_bytes(stream, 0x20, NULL, error);

    g_print("type: 0x%x, size: %u\n", (guint32)type, (guint32)size);
    printGBytes(hash);

    if(type == RSP6_END) break;

    data = g_input_stream_read_bytes(stream, size, NULL, error);

    g_assert(validity90_check_hash(hash, data));

    printGBytes(data);

    switch(type){
      case RSP6_TLS_CERT:
        ecdsa_q = validity90_handle_rsp6_pubkey_packet(data, error);
        info->tls_cert_raw = g_bytes_unref_to_array(data);
        break;
      case RSP6_ECDSA_PRIV_ENCRYPTED:
        ecdsa_d = validity90_handle_rsp6_ecdsa_packet(data, error);
        break;
      case RSP6_ECDH_PUB:
        ecdh = validity90_handle_rsp6_pubkey_packet(data, error);
        break;
      case RSP6_UNKNOWN_0:
      case RSP6_UNKNOWN_1:
      case RSP6_UNKNOWN_2:
      case RSP6_UNKNOWN_5:
        break;
      default:
        g_assert(FALSE);
        break;
    }
  }
  g_assert(ecdsa_q);
  g_assert(ecdsa_d);
  g_assert(ecdh);

  info->tls_client_privkey = g_byte_array_new();
  g_byte_array_append(info->tls_client_privkey, ecdsa_q->data, ecdsa_q->len);
  g_byte_array_append(info->tls_client_privkey, ecdsa_d->data, ecdsa_d->len);

  info->tls_server_pubkey = g_byte_array_new();
  g_byte_array_append(info->tls_server_pubkey, ecdh->data, ecdh->len);
}

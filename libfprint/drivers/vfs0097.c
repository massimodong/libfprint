#define FP_COMPONENT "vfs0097"

#define EP_IN (1 | FPI_USB_ENDPOINT_IN)
#define EP_OUT (1 | FPI_USB_ENDPOINT_OUT)
#define EP_INTERRUPT (3 | FPI_USB_ENDPOINT_IN)
#define TIMEOUT 5000

#include "drivers_api.h"
#include "vfs0097.h"
#include "validity90.h"
#include <openssl/ec.h>
#include <openssl/evp.h>
#include <openssl/obj_mac.h>
#include <openssl/err.h>
#include <openssl/x509.h>
#include <openssl/aes.h>
#include <openssl/ssl.h>
#include <openssl/tls1.h>
#include <openssl/ecdh.h>

struct _FpiDeviceVfs0097
{
  FpDevice parent;

  GByteArray *tls_certificate, *ecdsa_private_key, *pubkey, *key_block;
  guint8 *client_hello;
};

G_DECLARE_FINAL_TYPE (FpiDeviceVfs0097, fpi_device_vfs0097, FPI,
                      DEVICE_VFS0097, FpDevice);
G_DEFINE_TYPE (FpiDeviceVfs0097, fpi_device_vfs0097, FP_TYPE_DEVICE);

static void
mac_then_encrypt(guint8 type, const guint8 * data, int data_len, guint8 *key_block, guint8 **res, int *res_len) {
    guint8 iv[0x10] = {0x4b, 0x77, 0x62, 0xff, 0xa9, 0x03, 0xc1, 0x1e, 0x6f, 0xd8, 0x35, 0x93, 0x17, 0x2d, 0x54, 0xef};

    int prefix_len = 5;
    if (type == 0xFF) {
        prefix_len = 0;
    }
    // header for hmac + data + hmac
    guint8 all_data[prefix_len + data_len + 0x20];
    all_data[0] = type; all_data[1] = all_data[2] = 0x03; all_data[3] = (data_len >> 8) & 0xFF; all_data[4] = data_len & 0xFF;
    memcpy(all_data + prefix_len, data, data_len);

    gsize digest_len = 0x20;
    GHmac *ghmac = g_hmac_new(G_CHECKSUM_SHA256, key_block, 0x20);
    g_hmac_update(ghmac, all_data, prefix_len + data_len);
    g_hmac_get_digest(ghmac, all_data + prefix_len + data_len, &digest_len);
    g_assert(digest_len == 0x20);


    EVP_CIPHER_CTX *context = EVP_CIPHER_CTX_new();
    EVP_EncryptInit(context, EVP_aes_256_cbc(), key_block + 0x40, iv);
    EVP_CIPHER_CTX_set_padding(context, 0);

    *res_len = ((data_len + 16) / 16) * 16 + 0x30;
    *res = malloc(*res_len);
    memcpy(*res, iv, 0x10);
    int written = 0, wr2, wr3 = 0;

//    puts("To encrypt & mac:");
//    print_hex(data, data_len);

    EVP_EncryptUpdate(context, *res + 0x10, &written, all_data + prefix_len, data_len + 0x20);
//    printf("enc written: %03x\n", written);

    int pad_len = *res_len - (0x30 + data_len);
    if (pad_len == 0) {
        pad_len = 16;
    }
    guint8 pad[pad_len];
    memset(pad, pad_len - 1, pad_len);

    EVP_EncryptUpdate(context, *res + 0x10 + written, &wr3, pad, pad_len);

    EVP_EncryptFinal(context, *res + 0x10 + written + wr3, &wr2);
//    printf("enc written: %02x\n", wr2);
    *res_len = written + wr2 + wr3 + 0x10;

//    print_hex(all_data + prefix_len, data_len + 0x20);

//    puts("Encrypted& hmac");
//    print_hex(*res, *res_len);

    EVP_CIPHER_CTX_free(context);
}

static void
send_sequence(const guint8 seq[], gsize len, FpiSsm *ssm, FpDevice *dev)
{
  FpiUsbTransfer *transfer = fpi_usb_transfer_new (dev);
  fpi_usb_transfer_fill_bulk (transfer, EP_OUT, len);
  memcpy(transfer->buffer, seq, len);
  transfer->ssm = ssm;
  /*
  fpi_usb_transfer_submit (transfer, TIMEOUT, NULL, fpi_ssm_usb_transfer_cb, NULL);
  */
  GError *gerror = NULL;
  fpi_usb_transfer_submit_sync (transfer, TIMEOUT, &gerror);
  fpi_ssm_usb_transfer_cb(transfer, dev, NULL, gerror);
  fpi_usb_transfer_unref(transfer);
}

static void
tls_write(const guint8 *data, gsize len, FpiSsm *ssm, FpDevice *dev){
  FpiDeviceVfs0097 *vfsdev = FPI_DEVICE_VFS0097(dev);
  guint8 *res;
  int res_len;
  mac_then_encrypt(0x17, data, len, vfsdev->key_block->data, &res, &res_len);
  guint8 *wr = malloc(res_len + 5);
  memcpy(wr + 5, res, res_len);
  wr[0] = 0x17; wr[1] = wr[2] = 0x03; wr[3] = res_len >> 8; wr[4] = res_len & 0xFF;
  send_sequence(wr, res_len + 5, ssm, dev);

  free(res);
  free(wr);
}

#define send_init_sequence(seq) \
  send_sequence(seq, sizeof(seq), ssm, dev);

static void
TestTest (FpiUsbTransfer *transfer, FpDevice *device, gpointer data, GError *error)
{
  FILE *f = fopen("/home/massimo/W/a.dat", "a");
  fwrite(&transfer->actual_length, sizeof(transfer->actual_length), 1, f);
  fwrite(transfer->buffer, sizeof(guint8), transfer->actual_length, f);
  fclose(f);

  FpiUsbTransferCallback cb = data;
  cb(transfer, device, NULL, error);
}


static void
recv_sequence(gsize len, gboolean short_is_error, FpiSsm *ssm, FpDevice *dev, FpiUsbTransferCallback callback)
{
  FpiUsbTransfer *transfer = fpi_usb_transfer_new (dev);
  fpi_usb_transfer_fill_bulk (transfer, EP_IN, len);
  transfer->ssm = ssm;
  transfer->short_is_error = short_is_error;
  /*
  fpi_usb_transfer_submit (transfer, TIMEOUT, NULL, TestTest, callback);
  */
  GError *gerror = NULL;
  fpi_usb_transfer_submit_sync (transfer, TIMEOUT, &gerror);
  TestTest(transfer, dev, callback, gerror);
  fpi_usb_transfer_unref(transfer);

}

static void
recv_interrupt(gsize len, gboolean short_is_error, FpiSsm *ssm, FpDevice *dev, FpiUsbTransferCallback callback)
{
  FpiUsbTransfer *transfer = fpi_usb_transfer_new (dev);
  fpi_usb_transfer_fill_interrupt (transfer, EP_INTERRUPT, len);
  transfer->ssm = ssm;
  transfer->short_is_error = short_is_error;
  /*
  fpi_usb_transfer_submit (transfer, 0, NULL, callback, NULL);
  */
  GError *gerror = NULL;
  fpi_usb_transfer_submit_sync (transfer, 0, &gerror);
  callback(transfer, dev, NULL, gerror);
  fpi_usb_transfer_unref(transfer);
}

#define recv_init_sequence(len) \
  recv_sequence(len, FALSE, ssm, dev, fpi_ssm_usb_transfer_cb);

static void
initsm_init_keys_cb (FpiUsbTransfer *transfer, FpDevice *device, gpointer unused_data, GError *error)
{
  GInputStream *stream = g_memory_input_stream_new_from_data(transfer->buffer, transfer->actual_length, NULL);
  rsp6_info info;
  validity90_parse_rsp6(stream, &info, &error);

  FpiDeviceVfs0097 *vfsdev = FPI_DEVICE_VFS0097(device);
  memcpy(vfsdev->tls_certificate->data + 21, info.tls_cert_raw->data, info.tls_cert_raw->len);
  vfsdev->ecdsa_private_key = info.tls_client_privkey;
  vfsdev->pubkey = info.tls_server_pubkey;

  g_print("tls_certificate %d:\n", vfsdev->tls_certificate->len);
  for(int i=0;i<vfsdev->tls_certificate->len;i += 0x10){
    for(int j=0;j<0x10 && i + j < vfsdev->tls_certificate->len ;++j) g_print("%x\t", vfsdev->tls_certificate->data[i+j]);
    g_print("\n");
  }

  fpi_ssm_usb_transfer_cb(transfer, device, unused_data, error);
}

static guint8
*sign2(EC_KEY* key, guint8 *data, int data_len) {
    int len = 0;
    guint8 * res;
    do {
        ECDSA_SIG *sig = ECDSA_do_sign(data, data_len, key);
        len = i2d_ECDSA_SIG(sig, NULL);

        res = malloc(len);
        guint8 *f = res;
        i2d_ECDSA_SIG(sig, &f);
    } while (len != 0x48);

/*
    // test check
    char packet_bytes[] = {
      0x30, 0x46, 0x02, 0x21, 0x00, 0xa3, 0xad, 0xaa, 0x61,
      0x00, 0xe6, 0x9d, 0xbd, 0xcf, 0x48, 0x73, 0xb7,
      0xa6, 0xed, 0xe3, 0x62, 0x0a, 0x79, 0xe4, 0xf8,
      0x14, 0x27, 0x4d, 0xeb, 0x73, 0x91, 0x01, 0x0c,
      0xae, 0x08, 0xb9, 0x43, 0x02, 0x21, 0x00, 0xd3,
      0x28, 0xa4, 0x86, 0xcf, 0x8b, 0xaf, 0x35, 0xc9,
      0x04, 0xf7, 0x1f, 0xe2, 0x56, 0x22, 0xf7, 0x5d,
      0xdf, 0x53, 0x13, 0x4f, 0xc6, 0xdb, 0x6b, 0xc0,
      0x0d, 0x57, 0x90, 0xc4, 0x23, 0xfe, 0x06
    };
    char *test = malloc(sizeof(packet_bytes));
    memcpy(test, packet_bytes, sizeof(packet_bytes));
    char* pp = packet_bytes;

    ECDSA_SIG *sig2 = d2i_ECDSA_SIG(NULL, &pp, 0x48);
*/
//    int status = ECDSA_do_verify(data, data_len, sig2, load_key(ecdsa_private_key, false));
//    printf("Verified: %d", status);
//    if (status == 1) {
//    return test;
//    } else {
//        exit(-1);
//    }

    return res;
}


static EC_KEY *
load_key(const guint8 *data, gboolean is_private) {
    BIGNUM *x = BN_bin2bn(data, 0x20, NULL);
    BIGNUM *y = BN_bin2bn(data + 0x20, 0x20, NULL);
    BIGNUM *d = NULL;
    EC_KEY *key = EC_KEY_new_by_curve_name(NID_X9_62_prime256v1);

    if (!EC_KEY_set_public_key_affine_coordinates(key, x, y)) {
        goto err;
    }

    if (is_private) {
        d = BN_bin2bn(data + 0x40, 0x20, NULL);
        if (!EC_KEY_set_private_key(key, d)) {
            goto err;
        }
    }
    g_assert(EC_KEY_check_key(key) == 1);

    goto clean;

err:
    if (key) EC_KEY_free(key);
    key = NULL;
    ERR_print_errors_fp(stderr);

clean:
    if (x) BN_free(x);
    if (y) BN_free(y);
    if (d) BN_free(d);

    return key;
}

static const guchar client_random[] = {
  0x95, 0x6c, 0x41, 0xa9, 0x12, 0x86, 0x8a, 0xda,
  0x9b, 0xb2, 0x5b, 0xb4, 0xbb, 0xd6, 0x1d, 0xde,
  0x4f, 0xda, 0x23, 0x2a, 0x74, 0x7b, 0x2a, 0x93,
  0xf8, 0xac, 0xc6, 0x69, 0x24, 0x70, 0xc4, 0x2a
};

static void
handshake_rcv_hello_cb (FpiUsbTransfer *transfer, FpDevice *device, gpointer unused_data, GError *error)
{
  FpiDeviceVfs0097 *vfsdev = FPI_DEVICE_VFS0097(device);
  GChecksum *gchecksum1 = g_checksum_new(G_CHECKSUM_SHA256), *gchecksum2 = g_checksum_new(G_CHECKSUM_SHA256);

  g_print("::recv length: %ld\n", transfer->actual_length);

  const guint8 *server_random = transfer->buffer + 0xb;

  /*
  g_print("server random:\n");
  for(int i=0;i<0x20;++i){
    g_print("%x\t", server_random[i]);
  }
  g_print("\n");
  */

  g_checksum_update(gchecksum1, vfsdev->client_hello + 0x09, 0x43);
  g_checksum_update(gchecksum2, vfsdev->client_hello + 0x09, 0x43);
  g_checksum_update(gchecksum1, transfer->buffer + 0x05, 0x3d);
  g_checksum_update(gchecksum2, transfer->buffer + 0x05, 0x3d);

  EC_KEY *priv_key = load_key(privkey1, TRUE);
  EC_KEY *pub_key = load_key(vfsdev->pubkey->data, FALSE);
  int status;

  if (!priv_key || !pub_key) {
    /* TODO
    fpi_ssm_mark_failed (transfer->ssm, );
    return;
    */
    g_assert(0);
  }

  EVP_PKEY *priv = EVP_PKEY_new(), *pub = EVP_PKEY_new();
  status = EVP_PKEY_set1_EC_KEY(priv, priv_key);
  status = EVP_PKEY_set1_EC_KEY(pub, pub_key);

  EVP_PKEY_CTX *ctx = EVP_PKEY_CTX_new(priv, NULL);

  status = EVP_PKEY_derive_init(ctx);
  status = EVP_PKEY_derive_set_peer(ctx, pub);

  size_t len2 = 0;
  status = EVP_PKEY_derive(ctx, NULL, &len2);

  guint8 pre_master_secret[len2];

  ECDH_compute_key(pre_master_secret, 0x20, EC_KEY_get0_public_key(pub_key), priv_key, NULL);

  g_print("pre_master_secret: %lu\n", len2);
  for(int i=0;i<0x2;++i){
    for(int j=0;j<0x10;++j){
      g_print("%x\t", pre_master_secret[i * 0x10 + j]);
    }
    g_print("\n");
  }

  guint8 seed[0x40], expansion_seed[0x40];
  memcpy(seed, client_random, 0x20);
  memcpy(seed + 0x20, server_random, 0x20);

  memcpy(expansion_seed + 0x20, client_random, 0x20);
  memcpy(expansion_seed, server_random, 0x20);

  g_print("##seed:\n");
  for(int i=0;i<0x4;++i){
    for(int j=0;j<0x10;++j){
      g_print("%x\t", seed[i * 0x10 + j]);
    }
    g_print("\n");
  }

  GByteArray *master_secret = validity90_tls_prf(pre_master_secret, 0x20, "master secret", seed, 0x40, 0x30, &error);
  g_assert(master_secret->len == 0x30);

  g_print("master_secret:\n");
  for(int i=0;i<0x3;++i){
    for(int j=0;j<0x10;++j){
      g_print("%x\t", master_secret->data[i * 0x10 + j]);
    }
    g_print("\n");
  }

  GByteArray *key_block = validity90_tls_prf(master_secret->data, 0x30, "key expansion", seed, 0x40, 0x120, &error);
  g_assert(key_block->len == 0x120);
  vfsdev->key_block = key_block;

  g_print("Key block:\n");
  for(int i=0;i<0x12;++i){
    for(int j=0;j<0x10;++j){
      g_print("%x\t", key_block->data[i * 0x10 + j]);
    }
    g_print("\n");
  }

  memcpy(vfsdev->tls_certificate->data + 0xce + 4, privkey1, 0x40);

  g_checksum_update(gchecksum1, vfsdev->tls_certificate->data + 0x09, 0x109);
  g_checksum_update(gchecksum2, vfsdev->tls_certificate->data + 0x09, 0x109);

  guint8 test[0x20];
  gsize test_len = 0x20;
  g_print("test:\n");
  for(int i=0;i<0x20;++i) g_print("%x\t", test[i]);
  g_print("\n");

  g_checksum_get_digest(gchecksum1, test, &test_len);
  g_assert(test_len == 0x20);



  guint8* cert_verify_signature = sign2(load_key(vfsdev->ecdsa_private_key->data, TRUE), test, 0x20);

  //printf("\nCert signed: \n");
  //print_hex(cert_verify_signature, 0x48);
  memcpy(vfsdev->tls_certificate->data + 0x09 + 0x109 + 0x04, cert_verify_signature, 0x48);

  //    printf("\nWhat signed: \n");
  //    print_hex(all_messages, all_messages_index);

  // encrypted finished

  guint8 handshake_messages[0x20];
  gsize len3 = 0x20;
  g_checksum_update(gchecksum2, vfsdev->tls_certificate->data + 0x09 + 0x109, 0x4c);
  g_checksum_get_digest(gchecksum2, handshake_messages, &len3);
  g_assert(len3 == 0x20);

  guint8 *finished_message = g_malloc(0x10);
  finished_message[0] = 0x14;
  finished_message[1] = finished_message[2] = 0x00;
  finished_message[3] = 0x0c;

  GByteArray *finished_message2 = validity90_tls_prf(master_secret->data, 0x30, "client finished", handshake_messages, 0x20, 0x0c, /*finished_message + 0x04,*/ &error);
  g_assert(finished_message2->len == 0x0c);
  memcpy(finished_message + 0x04, finished_message2->data, finished_message2->len);
  // copy handshake protocol

  guint8 * final;
  int final_size;
  mac_then_encrypt(0x16, finished_message, 0x10, key_block->data, &final, &final_size);
  g_print("final size: %d\n", final_size);
  memcpy(vfsdev->tls_certificate->data + 0x169, final, final_size);

  fpi_ssm_usb_transfer_cb(transfer, device, unused_data, error);
}

static void
handshake_rcv_cert_cb (FpiUsbTransfer *transfer, FpDevice *device, gpointer unused_data, GError *error)
{
  g_print("recv len: %ld\n", transfer->length);
  g_print("recv actual len: %ld\n", transfer->actual_length);
  fpi_ssm_usb_transfer_cb(transfer, device, unused_data, error);
}

static void
handshakesm_run_state (FpiSsm *ssm, FpDevice *dev)
{
  FpiDeviceVfs0097 *vfsdev = FPI_DEVICE_VFS0097(dev);
  switch(fpi_ssm_get_cur_state(ssm)){
    case HANDSHAKE_STATE_CLIENT_HELLO:
      vfsdev->client_hello = g_malloc(TLS_CLIENT_HELLO_LEN);
      memcpy(vfsdev->client_hello, TLS_CLIENT_HELLO, TLS_CLIENT_HELLO_LEN);
      memcpy(vfsdev->client_hello + 0xF, client_random, 0x20);

      send_sequence(vfsdev->client_hello, TLS_CLIENT_HELLO_LEN, ssm, dev);
      break;
    case HANDSHAKE_STATE_SERVER_HELLO_RCV:
      recv_sequence(1024 * 1024, FALSE, ssm, dev, handshake_rcv_hello_cb);
      break;
    case HANDSHAKE_GENERATE_CERT:
      fpi_ssm_next_state (ssm);
      break;
    case HANDSHAKE_STATE_SEND_CERT:
      g_print("tls_certificate %d:\n", vfsdev->tls_certificate->len);
      for(int i=0;i<vfsdev->tls_certificate->len;i += 0x10){
        for(int j=0;j<0x10 && i + j < vfsdev->tls_certificate->len ;++j) g_print("%x\t", vfsdev->tls_certificate->data[i+j]);
        g_print("\n");
      }
      send_sequence(vfsdev->tls_certificate->data, vfsdev->tls_certificate->len, ssm, dev);
      break;
    case HANDSHAKE_STATE_CERT_REPLY:
      recv_sequence(65536, FALSE, ssm, dev, handshake_rcv_cert_cb);
      break;
  }
}

static void
initsm_run_state (FpiSsm *ssm, FpDevice *dev)
{
  FpiSsm *handshake_ssm;
  g_return_if_fail (ssm);
  switch(fpi_ssm_get_cur_state(ssm)){
    case INIT_STATE_GENERATE_MAIN_SEED:
      fpi_ssm_next_state (ssm);
      break;
    case INIT_STATE_SEQ_1:
      send_init_sequence(INIT_SEQUENCE_MSG1);
      break;
    case INIT_STATE_SEQ_1R:
      recv_init_sequence(INIT_SEQUENCE_RSP_SIZE1);
      break;
    case INIT_STATE_SEQ_2:
      send_init_sequence(INIT_SEQUENCE_MSG2);
      break;
    case INIT_STATE_SEQ_2R:
      recv_init_sequence(INIT_SEQUENCE_RSP_SIZE2);
      break;
    case INIT_STATE_SEQ_3:
      send_init_sequence(INIT_SEQUENCE_MSG3);
      break;
    case INIT_STATE_SEQ_3R:
      recv_init_sequence(INIT_SEQUENCE_RSP_SIZE3);
      break;
    case INIT_STATE_SEQ_4:
      send_init_sequence(INIT_SEQUENCE_MSG4);
      break;
    case INIT_STATE_SEQ_4R:
      recv_init_sequence(INIT_SEQUENCE_RSP_SIZE4);
      break;
    case INIT_STATE_SEQ_5:
      send_init_sequence(INIT_SEQUENCE_MSG5);
      break;
    case INIT_STATE_SEQ_5R:
      recv_init_sequence(INIT_SEQUENCE_RSP_SIZE5);
      break;
    case INIT_STATE_SEQ_6:
      send_init_sequence(INIT_SEQUENCE_MSG6);
      break;
    case INIT_STATE_KEYS:
      recv_sequence(INIT_SEQUENCE_RSP_SIZE6, TRUE, ssm, dev, initsm_init_keys_cb);
      break;
    case INIT_STATE_HANDSHAKE:
      handshake_ssm = fpi_ssm_new(dev, handshakesm_run_state, HANDSHAKESM_NUM_STATES);
      fpi_ssm_start_subsm(ssm, handshake_ssm);
      break;
  }
}

static void
initsm_done (FpiSsm *ssm, FpDevice *dev, GError *error)
{
  fpi_device_open_complete(dev, error);
}

static void
dev_open (FpDevice *dev)
{
  GError *gerror = NULL;
  g_usb_device_reset(fpi_device_get_usb_device(dev), &gerror);
  FpiSsm *ssm;
  ssm = fpi_ssm_new (dev, initsm_run_state, INITSM_NUM_STATES);
  fpi_ssm_start (ssm, initsm_done);
}

static void
dev_close (FpDevice *dev)
{
  fpi_device_close_complete(dev, NULL);
}

static int
detect_interrupt(guint8 *interrupt, gsize interrupt_len)
{
    const guint8 waiting_finger[] = { 0x00, 0x00, 0x00, 0x00, 0x00 };
    const guint8 finger_down[] = { 0x02, 0x00, 0x40, 0x10, 0x00 };
    const guint8 finger_down2[] = { 0x02, 0x00, 0x40, 0x06, 0x06 };
    const guint8 scanning_prints[] = { 0x03, 0x40, 0x01, 0x00, 0x00 };
    const guint8 scan_completed[] = { 0x03, 0x41, 0x03, 0x00, 0x40 };

    const guint8 desired_interrupt[] = { 0x03, 0x43, 0x04, 0x00, 0x41 };
    const guint8 desired_interrupt_v97[] = { 0x03, 0x42, 0x04, 0x00, 0x40 };
    const guint8 low_quality_scan_interrupt[] = { 0x03, 0x42, 0x04, 0x00, 0x40 };
    const guint8 scan_failed_too_short_interrupt[] = { 0x03, 0x60, 0x07, 0x00, 0x40 };
    const guint8 scan_failed_too_short2_interrupt[] = { 0x03, 0x61, 0x07, 0x00, 0x41 };
    const guint8 scan_failed_too_fast_interrupt[] = { 0x03, 0x20, 0x07, 0x00, 0x00 };

    if (sizeof(waiting_finger) == interrupt_len &&
        memcmp(waiting_finger, interrupt, interrupt_len) == 0) {
      return VERIFY_INTERRUPT_WAIT;
    }
    if ((sizeof(finger_down) == interrupt_len &&
          memcmp(finger_down, interrupt, interrupt_len) == 0) ||
        (sizeof(finger_down2) == interrupt_len &&
         memcmp(finger_down2, interrupt, interrupt_len) == 0)) {
      return VERIFY_INTERRUPT_FINGER_DOWN;
    }
    if (sizeof(scanning_prints) == interrupt_len &&
        memcmp(scanning_prints, interrupt, interrupt_len) == 0) {
      return VERIFY_INTERRUPT_SCANNING;
    }
    if (sizeof(scan_completed) == interrupt_len &&
        memcmp(scan_completed, interrupt, interrupt_len) == 0) {
      return VERIFY_INTERRUPT_SCAN_COMPLETE;
    }
    if (sizeof(scan_failed_too_short_interrupt) == interrupt_len &&
        memcmp(scan_failed_too_short_interrupt, interrupt, interrupt_len) == 0) {
      return VERIFY_INTERRUPT_SCAN_TOO_SHORT;
    }
    if (sizeof(scan_failed_too_short2_interrupt) == interrupt_len &&
        memcmp(scan_failed_too_short2_interrupt, interrupt, interrupt_len) == 0) {
      return VERIFY_INTERRUPT_SCAN_TOO_SHORT;
    }
    if (sizeof(scan_failed_too_fast_interrupt) == interrupt_len &&
        memcmp(scan_failed_too_fast_interrupt, interrupt, interrupt_len) == 0) {
      return VERIFY_INTERRUPT_SCAN_TOO_FAST;
    }
    if (sizeof(desired_interrupt) == interrupt_len &&
        memcmp(desired_interrupt, interrupt, interrupt_len) == 0) {
      return VERIFY_INTERRUPT_SCAN_SUCCEED;
    }
    if (sizeof(desired_interrupt_v97) == interrupt_len &&
        memcmp(desired_interrupt_v97, interrupt, interrupt_len) == 0) {
      return VERIFY_INTERRUPT_SCAN_SUCCEED;
    }
    if (sizeof(low_quality_scan_interrupt) == interrupt_len &&
        memcmp(low_quality_scan_interrupt, interrupt, interrupt_len) == 0) {
      return VERIFY_INTERRUPT_SCAN_SUCCEED;
    }

    return VERIFY_INTERRUPT_UNKNOWN;
}

static void
enroll_interrupt_cb (FpiUsbTransfer *transfer, FpDevice *device, gpointer unused_data, GError *error)
{
  if(error){
    fpi_ssm_mark_failed(transfer->ssm, error);
    return;
  }
  g_print("read interrupt:\n");
  for(int i=0;i<transfer->actual_length;++i) g_print("%x\t", transfer->buffer[i]);
  g_print("\n");
  int interrupt = detect_interrupt(transfer->buffer, transfer->actual_length);
  g_print("interrupt: %d\n", interrupt);
  switch(interrupt){
    case VERIFY_INTERRUPT_WAIT:
    case VERIFY_INTERRUPT_FINGER_DOWN:
    case VERIFY_INTERRUPT_SCANNING:
    case VERIFY_INTERRUPT_SCAN_COMPLETE:
    case VERIFY_INTERRUPT_UNKNOWN:
      fpi_ssm_jump_to_state (transfer->ssm, VERIFY_STATE_INTERRUPTS);
      break;
    case VERIFY_INTERRUPT_SCAN_TOO_SHORT:
    case VERIFY_INTERRUPT_SCAN_TOO_FAST:
      fpi_device_enroll_complete(device, NULL, fpi_device_retry_new(FP_DEVICE_RETRY_TOO_SHORT));
      fpi_ssm_mark_completed(transfer->ssm);
      break;
    case VERIFY_INTERRUPT_SCAN_SUCCEED:
      /*
      fpi_device_get_enroll_data(device, &print);
      fp_data = g_variant_new_int32(1);
      g_object_set (print, "fpi-data", fp_data, NULL);
      fpi_device_enroll_complete(device, print, NULL);
      */
      fpi_ssm_next_state (transfer->ssm);
      break;
  }
}

static void
enroll_print_id_cb (FpiUsbTransfer *transfer, FpDevice *device, gpointer unused_data, GError *error)
{
  if(error){
    fpi_ssm_mark_failed(transfer->ssm, error);
    return;
  }
  guint8 validated_finger_id = transfer->buffer[0] == 0x03 ? transfer->buffer[2] : 0;
  if(validated_finger_id == 0){
    fpi_device_enroll_complete(device, NULL, fpi_device_retry_new(FP_DEVICE_RETRY_GENERAL));
  }else{
    FpPrint *print;
    GVariant *fp_data;
    fpi_device_get_enroll_data(device, &print);
    fpi_print_set_type (print, FPI_PRINT_RAW);
    fp_data = g_variant_new_byte(validated_finger_id);
    g_object_set (print, "fpi-data", fp_data, NULL);
    fpi_device_enroll_complete(device, print, NULL);
  }
}

static void
enrollsm_run_state (FpiSsm *ssm, FpDevice *dev)
{
  switch(fpi_ssm_get_cur_state(ssm)){
    case VERIFY_STATE_GREEN_LIGHT:
      tls_write(LED_GREEN_ON, sizeof(LED_GREEN_ON), ssm, dev);
      break;
    case VERIFY_STATE_GREEN_LIGHT_RECV:
      recv_sequence(65536, FALSE, ssm, dev, fpi_ssm_usb_transfer_cb);
      break;
    case VERIFY_STATE_SEND_MATRIX:
      tls_write(V97_SCAN_MATRIX2, sizeof(V97_SCAN_MATRIX2), ssm, dev);
      break;
    case VERIFY_STATE_SEND_MATRIX_RECV:
      recv_sequence(65536, FALSE, ssm, dev, fpi_ssm_usb_transfer_cb);
      break;
    case VERIFY_STATE_INTERRUPTS:
      recv_interrupt(0x100, FALSE, ssm, dev, enroll_interrupt_cb);
      break;
    case VERIFY_STATE_SEND_CHECK_DB:
      tls_write(CHECK_DB_PACKET, sizeof(CHECK_DB_PACKET), ssm, dev);
      break;
    case VERIFY_STATE_SEND_CHECK_DB_RECV:
      recv_sequence(65536, FALSE, ssm, dev, fpi_ssm_usb_transfer_cb);
      break;
    case VERIFY_STATE_PRINT_ID:
      recv_interrupt(0x100, FALSE, ssm, dev, enroll_print_id_cb);
      break;
    default:
      g_assert(0); //TODO
  }
}

static void
enrollsm_done (FpiSsm *ssm, FpDevice *dev, GError *error)
{
  //do nothing
}

static void
dev_enroll (FpDevice *dev)
{
  FpiSsm *ssm;
  ssm = fpi_ssm_new(dev, enrollsm_run_state, VERIFYSM_NUM_STATES);
  fpi_ssm_start (ssm, enrollsm_done);
}

static void
verify_interrupt_cb (FpiUsbTransfer *transfer, FpDevice *device, gpointer unused_data, GError *error)
{
  if(error){
    fpi_ssm_mark_failed(transfer->ssm, error);
    return;
  }
  g_print("read interrupt:\n");
  for(int i=0;i<transfer->actual_length;++i) g_print("%x\t", transfer->buffer[i]);
  g_print("\n");
  int interrupt = detect_interrupt(transfer->buffer, transfer->actual_length);
  g_print("interrupt: %d\n", interrupt);
  switch(interrupt){
    case VERIFY_INTERRUPT_WAIT:
    case VERIFY_INTERRUPT_FINGER_DOWN:
    case VERIFY_INTERRUPT_SCANNING:
    case VERIFY_INTERRUPT_SCAN_COMPLETE:
    case VERIFY_INTERRUPT_UNKNOWN:
      fpi_ssm_jump_to_state (transfer->ssm, VERIFY_STATE_INTERRUPTS);
      break;
    case VERIFY_INTERRUPT_SCAN_TOO_SHORT:
    case VERIFY_INTERRUPT_SCAN_TOO_FAST:
      fpi_device_verify_report(device, FPI_MATCH_ERROR, NULL, fpi_device_retry_new(FP_DEVICE_RETRY_TOO_SHORT));
      fpi_device_verify_complete(device, NULL);
      fpi_ssm_mark_completed(transfer->ssm);
      break;
    case VERIFY_INTERRUPT_SCAN_SUCCEED:
      /*
      fpi_device_get_enroll_data(device, &print);
      fp_data = g_variant_new_int32(1);
      g_object_set (print, "fpi-data", fp_data, NULL);
      fpi_device_enroll_complete(device, print, NULL);
      */
      fpi_ssm_next_state (transfer->ssm);
      break;
  }
}

static void
verify_print_id_cb (FpiUsbTransfer *transfer, FpDevice *device, gpointer unused_data, GError *error)
{
  if(error){
    fpi_ssm_mark_failed(transfer->ssm, error);
    return;
  }
  guint8 validated_finger_id = transfer->buffer[0] == 0x03 ? transfer->buffer[2] : 0;
  if(validated_finger_id == 0){
    fpi_device_verify_report(device, FPI_MATCH_FAIL, NULL, NULL);
    fpi_device_verify_complete(device, NULL);
  }else{
    fpi_device_verify_report(device, FPI_MATCH_SUCCESS, NULL, NULL);
    fpi_device_verify_complete(device, NULL);
  }
}

static void
verifysm_run_state (FpiSsm *ssm, FpDevice *dev)
{
  switch(fpi_ssm_get_cur_state(ssm)){
    case VERIFY_STATE_GREEN_LIGHT:
      tls_write(LED_GREEN_ON, sizeof(LED_GREEN_ON), ssm, dev);
      break;
    case VERIFY_STATE_GREEN_LIGHT_RECV:
      recv_sequence(65536, FALSE, ssm, dev, fpi_ssm_usb_transfer_cb);
      break;
    case VERIFY_STATE_SEND_MATRIX:
      tls_write(V97_SCAN_MATRIX2, sizeof(V97_SCAN_MATRIX2), ssm, dev);
      break;
    case VERIFY_STATE_SEND_MATRIX_RECV:
      recv_sequence(65536, FALSE, ssm, dev, fpi_ssm_usb_transfer_cb);
      break;
    case VERIFY_STATE_INTERRUPTS:
      recv_interrupt(0x100, FALSE, ssm, dev, verify_interrupt_cb);
      break;
    case VERIFY_STATE_SEND_CHECK_DB:
      tls_write(CHECK_DB_PACKET, sizeof(CHECK_DB_PACKET), ssm, dev);
      break;
    case VERIFY_STATE_SEND_CHECK_DB_RECV:
      recv_sequence(65536, FALSE, ssm, dev, fpi_ssm_usb_transfer_cb);
      break;
    case VERIFY_STATE_PRINT_ID:
      recv_interrupt(0x100, FALSE, ssm, dev, verify_print_id_cb);
      break;
    default:
      g_assert(0); //TODO
  }
}

static void
verifysm_done (FpiSsm *ssm, FpDevice *dev, GError *error)
{
  //do nothing
}

static void
dev_verify (FpDevice *dev)
{
  FpiSsm *ssm;
  ssm = fpi_ssm_new(dev, verifysm_run_state, VERIFYSM_NUM_STATES);
  fpi_ssm_start (ssm, verifysm_done);
}

static const FpIdEntry id_table[] = {
  { .vid = 0x138a, .pid = 0x0097, },
  { .vid = 0,  .pid = 0,  .driver_data = 0 },       /* terminating entry */
};

static void
fpi_device_vfs0097_init (FpiDeviceVfs0097 *self)
{
  self->tls_certificate = g_byte_array_new();
  g_byte_array_append(self->tls_certificate, TLS_CERTIFICATE_INIT, TLS_CERTIFICATE_INIT_LEN);
  self->ecdsa_private_key = NULL;
  self->pubkey = NULL;
}

static void
fpi_device_vfs0097_class_init (FpiDeviceVfs0097Class *klass)
{
  FpDeviceClass *dev_class = FP_DEVICE_CLASS(klass);

  dev_class->id = FP_COMPONENT;
  dev_class->full_name = "Validity VFS0050";

  dev_class->type = FP_DEVICE_TYPE_USB;
  dev_class->scan_type = FP_SCAN_TYPE_PRESS;
  dev_class->id_table = id_table;
  dev_class->nr_enroll_stages = 999; //TODO

  dev_class->open = dev_open;
  dev_class->close = dev_close;
  dev_class->verify = dev_verify;
  dev_class->enroll = dev_enroll;
}
